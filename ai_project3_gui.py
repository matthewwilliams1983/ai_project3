from tkinter import PhotoImage
import tkinter as tk


class MainWindow(tk.Tk):
    def __init__(self, *args, state=None, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        # Window properties
        self.title("Project_3_AI")
        self.geometry("1000x800")
        self.iconbitmap(default="AI_Images/windowsIconAI.ico")
        self.resizable(0, 0)

        # Create block height attributes for starting state
        self.block_a_y = 0
        self.block_b_y = 0
        self.block_c_y = 0
        self.block_d_y = 0
        self.block_e_y = 0
        self.block_f_y = 0
        self.block_g_y = 0
        self.block_h_y = 0
        self.block_i_y = 0
        self.block_j_y = 0
        self.define_height()

        self.current_state = state
        self.set_starting_blocks_x_axis()

        # Images of blocks
        self.block_a_image = PhotoImage(file="AI_Images/building_block_a_image.png")
        self.block_b_image = PhotoImage(file="AI_Images/building_block_b_image.png")
        self.block_c_image = PhotoImage(file="AI_Images/building_block_c_image.png")
        self.block_d_image = PhotoImage(file="AI_Images/building_block_d_image.png")
        self.block_e_image = PhotoImage(file="AI_Images/building_block_e_image.png")
        self.block_f_image = PhotoImage(file="AI_Images/building_block_f_image.png")
        self.block_g_image = PhotoImage(file="AI_Images/building_block_g_image.png")
        self.block_h_image = PhotoImage(file="AI_Images/building_block_h_image.png")
        self.block_i_image = PhotoImage(file="AI_Images/building_block_i_image.png")
        self.block_j_image = PhotoImage(file="AI_Images/building_block_j_image.png")

        # Canvas to place blocks on
        self.block_canvas = tk.Canvas(self, width=1000, height=800)
        self.block_canvas.config(borderwidth=0, highlightbackground='white', bg='white')
        self.block_canvas.place(relx=0.5, rely=0.5, anchor=tk.CENTER)

        # x 125, 350, 575, 800
        # y 780, 700, 620, 540, 460, 380, 300, 220, 140, 60
        self.block_a = self.block_canvas.create_image(125 + (225 * self.block_a_x), 780 - (self.block_a_y * 80),
                                                      image=self.block_a_image, anchor=tk.SW, tags='A')
        self.block_b = self.block_canvas.create_image(125 + (225 * self.block_b_x), 780 - (self.block_b_y * 80),
                                                      image=self.block_b_image, anchor=tk.SW, tags='B')
        self.block_c = self.block_canvas.create_image(125 + (225 * self.block_c_x), 780 - (self.block_c_y * 80),
                                                      image=self.block_c_image, anchor=tk.SW, tags='C')
        self.block_d = self.block_canvas.create_image(125 + (225 * self.block_d_x), 780 - (self.block_d_y * 80),
                                                      image=self.block_d_image, anchor=tk.SW, tags='D')
        self.block_e = self.block_canvas.create_image(125 + (225 * self.block_e_x), 780 - (self.block_e_y * 80),
                                                      image=self.block_e_image, anchor=tk.SW, tags='E')
        self.block_f = self.block_canvas.create_image(125 + (225 * self.block_f_x), 780 - (self.block_f_y * 80),
                                                      image=self.block_f_image, anchor=tk.SW, tags='F')
        self.block_g = self.block_canvas.create_image(125 + (225 * self.block_g_x), 780 - (self.block_g_y * 80),
                                                      image=self.block_g_image, anchor=tk.SW, tags='G')
        self.block_h = self.block_canvas.create_image(125 + (225 * self.block_h_x), 780 - (self.block_h_y * 80),
                                                      image=self.block_h_image, anchor=tk.SW, tags='H')
        self.block_i = self.block_canvas.create_image(125 + (225 * self.block_i_x), 780 - (self.block_i_y * 80),
                                                      image=self.block_i_image, anchor=tk.SW, tags='I')
        self.block_j = self.block_canvas.create_image(125 + (225 * self.block_j_x), 780 - (self.block_j_y * 80),
                                                      image=self.block_j_image, anchor=tk.SW, tags='J')

    # Set the block in the proper location in the x axis
    # Defining instance variables outside of init feels bad man
    def set_starting_blocks_x_axis(self):
        for x in range(len(self.current_state)):
            if self.current_state[x].block_name == 'A':
                self.block_a_x = self.current_state[x].location
            elif self.current_state[x].block_name == 'B':
                self.block_b_x = self.current_state[x].location
            elif self.current_state[x].block_name == 'C':
                self.block_c_x = self.current_state[x].location
            elif self.current_state[x].block_name == 'D':
                self.block_d_x = self.current_state[x].location
            elif self.current_state[x].block_name == 'E':
                self.block_e_x = self.current_state[x].location
            elif self.current_state[x].block_name == 'F':
                self.block_f_x = self.current_state[x].location
            elif self.current_state[x].block_name == 'G':
                self.block_g_x = self.current_state[x].location
            elif self.current_state[x].block_name == 'H':
                self.block_h_x = self.current_state[x].location
            elif self.current_state[x].block_name == 'I':
                self.block_i_x = self.current_state[x].location
            elif self.current_state[x].block_name == 'J':
                self.block_j_x = self.current_state[x].location

    # Set the height for the starting blocks
    # Not ideal but better than manual and a simple solution
    def define_height(self):
        file = open("start_state.txt", 'r')
        for line in file:
            block_state = line.rstrip('\n').split(',')
            if block_state[0] != '':
                for x in range(len(block_state)):
                    if block_state[x] == 'A':
                        self.block_a_y = x
                    elif block_state[x] == 'B':
                        self.block_b_y = x
                    elif block_state[x] == 'C':
                        self.block_c_y = x
                    elif block_state[x] == 'D':
                        self.block_d_y = x
                    elif block_state[x] == 'E':
                        self.block_e_y = x
                    elif block_state[x] == 'F':
                        self.block_f_y = x
                    elif block_state[x] == 'G':
                        self.block_g_y = x
                    elif block_state[x] == 'H':
                        self.block_h_y = x
                    elif block_state[x] == 'I':
                        self.block_i_y = x
                    elif block_state[x] == 'J':
                        self.block_j_y = x