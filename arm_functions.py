from block import Block
import time

ANIMATION_SPEED = 16
SLEEP_TIME = 0.025

class RobotArm:

    def __init__(self, start_state=None, final_state=None, gui=None):
        self.state = start_state
        self.final_state = final_state
        self.gui = gui

    ####################################################################################################################
    # This function operates the arm by name but really just calls work to be done on an individual column.
    #
    # @param: N/A
    #
    # @return: N/A
    #
    # Notes: Columns are offset by -1 so I can do modular operations easier (this was my thought process approaching it
    #        anyway.
    ####################################################################################################################
    def operate_arm(self):

        # Work column 1
        print("Working on column 1...")
        self.work_column(0)
        print("Finished column 1.")

        # Work column 2
        print("Working on column 2...")
        self.work_column(1)
        print("Finished column 2.")

        # Work column 3
        print("Working on column 3...")
        self.work_column(2)
        print("Finished column 3.")

        # Work column 4
        print("Working on column 4...")
        self.work_column(3)
        print("Finished column 4.")

        # Finished
        self.no_operation()

    ####################################################################################################################
    # This function pick ups a block and calls the unstack function or simply changes the on_table_state to False if the
    # picked up block was not stacked on another block.
    #
    # @param: picked_block Block: This is the block that is being picked up by the robot.
    #
    # @return: N/A
    #
    # Notes: Sleep for some amount of time here to watch print statements (will adjust time as graphics are implemented.
    ####################################################################################################################
    def pick_up(self, picked_block=Block):

        # Update Graphics
        while self.gui.block_canvas.coords(picked_block.block_name)[1] > 90:
            new_offset = self.gui.block_canvas.coords(picked_block.block_name)[1] - ANIMATION_SPEED
            self.gui.block_canvas.coords(picked_block.block_name, 125 + (225 * picked_block.location), new_offset)
            self.gui.update()
            time.sleep(SLEEP_TIME)

        # If there is a block below the picked up block, unstack
        if picked_block.on != 'None':
            self.unstack(picked_block)
        else: # The block was on the table just pick it up
            picked_block.on_table_state = False

        # Inform the user where the block was picked up from
        print("Picked up block " + picked_block.block_name + " from location L" + str(picked_block.location + 1))


    def put_down(self, picked_block=Block, location=None, table_state=False):

        is_stack_op = False
        count_in_stack = 0

        # Update Graphics
        if location > picked_block.location:
            while self.gui.block_canvas.coords(picked_block.block_name)[0] < (125 + (225 * location)):
                new_offset = self.gui.block_canvas.coords(picked_block.block_name)[0] + ANIMATION_SPEED
                if new_offset > (125 + (225 * location)):
                    new_offset = (125 + (225 * location))
                self.gui.block_canvas.coords(picked_block.block_name, new_offset, self.gui.block_canvas.coords(picked_block.block_name)[1])
                self.gui.update()
                time.sleep(SLEEP_TIME)
        else:
            while self.gui.block_canvas.coords(picked_block.block_name)[0] > (125 + (225 * location)):
                new_offset = self.gui.block_canvas.coords(picked_block.block_name)[0] - ANIMATION_SPEED
                if new_offset < (125 + (225 * location)):
                    new_offset = (125 + (225 * location))
                self.gui.block_canvas.coords(picked_block.block_name, new_offset,
                                             self.gui.block_canvas.coords(picked_block.block_name)[1])
                self.gui.update()
                time.sleep(SLEEP_TIME)

        for block in self.state:
            if block.location == location:
                count_in_stack += 1

        while self.gui.block_canvas.coords(picked_block.block_name)[1] < 780 - (count_in_stack * 80):
            new_offset = self.gui.block_canvas.coords(picked_block.block_name)[1] + ANIMATION_SPEED
            self.gui.block_canvas.coords(picked_block.block_name, self.gui.block_canvas.coords(picked_block.block_name)[0], new_offset)
            self.gui.update()
            time.sleep(SLEEP_TIME)

        for block in self.state:
            if block.location == location and block.clear_state and block != picked_block:
                self.stack(block_to_stack=picked_block, block_to_stack_on=block, location=location)
                is_stack_op = True
        if not is_stack_op:
            self.place_block_on_table(picked_block, location)
        print("Put down block " + picked_block.block_name + " on location L" + str(picked_block.location + 1))

    @staticmethod
    def place_block_on_table(picked_block=Block, location=None):

        picked_block.location = location
        picked_block.above = 'None'
        picked_block.on = 'None'
        picked_block.on_table_state = True
        picked_block.clear_state = True

    @staticmethod
    def stack(block_to_stack=Block, block_to_stack_on=Block, location=None):

        # Change attributes of moved block
        block_to_stack.location = location
        block_to_stack.on = block_to_stack_on.block_name
        block_to_stack.on_table_state = False

        # Change attributes of block that is being stacked on
        block_to_stack_on.above = block_to_stack.block_name
        block_to_stack_on.clear_state = False

    def unstack(self, picked_block=Block):

        for block in self.state:
            if block.block_name == picked_block.on:
                block.clear_state = True
                block.above = 'None'

        # Clear attributes associated with any other blocks for picked up block
        picked_block.on = 'None'

    def no_operation(self):
        print('\n')
        print("Finished...\nPrinting final block states...")
        print('\n\n-----------------------')
        for block in self.final_state:
            print(block)
            print('\n-----------------------')

    def work_column(self, current_column):

        # Sort column list so they can be compared
        final_state_column, state_column = self.find_column_state(current_column=current_column)
        final_state_column.sort(key=lambda x: x.block_name, reverse=False)
        state_column.sort(key=lambda  x: x.block_name, reverse=False)

        # Clear columns to begin work
        self.clear_current_column(current_column)

        table_block = self.manage_table_block(current_column)
        next_block = self.find_next_block(table_block)
        continue_running = True

        while state_column != final_state_column:
            for x in state_column:
                print("STATE: ", x)
            for x in final_state_column:
                print("FINAL:", x)
            if next_block is not None:
                while next_block.location != current_column:
                    for block in self.state:
                        if block.clear_state and block.location == next_block.location and block != next_block:
                            self.pick_up(block)
                            location_to_temp = (block.location + 1) % 3
                            if location_to_temp == current_column:
                                location_to_temp += 1
                            self.put_down(block, location_to_temp)
                        elif block.clear_state and block.block_name == next_block.block_name:
                            self.pick_up(block)
                            self.put_down(block, current_column)

            next_block = self.find_next_block(next_block)


    def find_column_state(self, current_column=None):

        final_state_column = []
        state_column = []

        for block in self.final_state:
            if block.location == current_column:
                final_state_column.append(block)

        for block in self.state:
            for blocks in final_state_column:
                if block.block_name == blocks.block_name:
                    state_column.append(block)

        return final_state_column, state_column

    def clear_current_column(self, current_column):

        is_column_clear = False

        for block in self.state:
            if block.location == current_column:
                is_column_clear = False
                break
            else:
                is_column_clear = True
        if is_column_clear:
            return

        for block in self.state:
            if block.location == current_column and block.clear_state:
                self.pick_up(block)
                location_to_temp = (block.location + 1) % 3
                if location_to_temp == current_column:
                    location_to_temp += 1
                self.put_down(block, location=location_to_temp)

        self.clear_current_column(current_column)

    def find_next_block(self, current_block):

        next_block = None
        find_next_block = False

        for block in self.final_state:
            if current_block is not None and block.block_name == current_block.block_name:
                if block.above != 'None':
                    for block_s in self.state:
                        if block_s.block_name == block.above:
                            next_block = block_s
                            find_next_block = True
            if find_next_block:
                break
            elif next_block is not None and block.block_name == next_block.block_name:
                if block.above != 'None':
                    for block_s in self.state:
                        if block_s.block_name == block.above:
                            next_block = block_s

        return next_block

    def manage_table_block(self, current_column):

        table_block = None
        block_to_put_on_table = None
        column_not_empty_in_final = False

        for block in self.final_state:
            if block.location == current_column and block.on_table_state:
                block_to_put_on_table = block
                column_not_empty_in_final = True
                break

        if column_not_empty_in_final:
            for block in self.state:
                if block.block_name == block_to_put_on_table.block_name:
                    block_in_state_to_put_on_table = block

            while block_in_state_to_put_on_table.location != current_column:
                for block in self.state:
                    if block.location == block_in_state_to_put_on_table.location and \
                            block.block_name != block_in_state_to_put_on_table.block_name and block.clear_state:
                        self.pick_up(block)
                        location_to_temp = (block.location + 1) % 3
                        if location_to_temp == current_column:
                            location_to_temp += 1
                        self.put_down(block, location=location_to_temp)
                    elif block.location == block_in_state_to_put_on_table.location and \
                            block.block_name == block_in_state_to_put_on_table.block_name and block.clear_state:
                        self.pick_up(block)

                        self.put_down(block, location=current_column, table_state=True)
                        table_block = block
                        break

        return table_block
