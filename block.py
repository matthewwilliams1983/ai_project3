
class Block:

    def __init__(self, block_name='None', location='None', start_above='None',
                 start_on='None', clear_state='None', on_table_state='None'):
        self.block_name = block_name
        self.location = location
        self.above = start_above
        self.on = start_on
        self.clear_state = clear_state
        self.on_table_state = on_table_state

    def __str__(self):

        block_rep = 'Block Name: ' + self.block_name + '\nLocation: L' + str(self.location + 1) \
                    + '\nAbove: ' + self.above + '\nOn: ' + self.on + '\nClear State: ' + str(self.clear_state) \
                    + '\nOn Table State: ' + str(self.on_table_state)
        return block_rep

    def __eq__(self, other):
        return self.__dict__ == other.__dict__
