from block import Block
from arm_functions import RobotArm
import time
from ai_project3_gui import MainWindow

NUM_BLOCKS = 10
START_FILE_NAME = 'start_state.txt'
END_FILE_NAME = 'end_state.txt'


def set_state_from_file(file_name, state_name):

    block_list = []
    file = open(file_name, 'r')
    location = 0

    print("________________" + state_name + "________________")
    for line in file:
        block_state = line.rstrip('\n').split(',')
        print(block_state)
        if block_state[0] != '':
            for x in range(len(block_state)):
                block_list.append(Block(block_name=block_state[x], location=location))


            for x in range(len(block_list) - len(block_state), len(block_list)):
                if x == len(block_list) - len(block_state):
                    block_list[x].on_table_state = True
                else:
                    block_list[x].on_table_state = False
                if len(block_list) > x + 1:
                    block_list[x].above = block_list[x + 1].block_name
                if x != len(block_list) - len(block_state):
                    block_list[x].on = block_list[x - 1].block_name
                else:
                    block_list
                if x == len(block_list) - 1:
                    block_list[x].clear_state = True
                else:
                    block_list[x].clear_state = False
        location += 1

    for block in block_list:
        print("NAME:", block.block_name)
        print("LOC:", block.location)
        print("TABLE:", block.on_table_state)
        print("ABOVE:", block.above)
        print("ON:", block.on)
        print("CLEAR:", block.clear_state)
        print("\n")

    print('____________________________________________')

    return block_list


def main():

    # Setup blocks from file
    block_list = set_state_from_file(START_FILE_NAME, "START STATE")
    final_block_list = set_state_from_file(END_FILE_NAME, "END STATE")

    # Create gui
    window = MainWindow(state=block_list)
    robot_arm = RobotArm(start_state=block_list, final_state=final_block_list, gui=window)
    window.after(1, robot_arm.operate_arm)
    window.mainloop()


if __name__ == '__main__':
    main()